package main

import (
	"archive/zip"
	"bufio"
	"flag"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/elazarl/goproxy"
)

func getDomains(file io.Reader) map[string]bool {
	domains := map[string]bool{}
	reader := bufio.NewReader(file)
	for {
		str, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		str = strings.Replace(str, "\n", "", -1)
		domains[str] = true
	}
	return domains
}

func getDomainsFromText(filename string) map[string]bool {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()
	return getDomains(file)
}
func getDomainsFromZip(filename string) map[string]bool {
	r, err := zip.OpenReader(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()
	for _, f := range r.File {
		file, err := f.Open()
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		return getDomains(file)
	}
	return nil
}

func main() {
	filename := flag.String("d", "", "file with all the domains in a list")
	flag.Parse()
	log.Println("Reading domains ")
	domains := map[string]bool{}
	if len(*filename) > 0 {
		domains = getDomainsFromText(*filename)
	} else {
		dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			log.Fatal(err)
		}
		domains = getDomainsFromZip(dir + "/domains.zip")
	}
	log.Println("Number of domains: ", len(domains))
	port := ":3128"
	log.Println("Starting the proxy on " + port)
	proxy := goproxy.NewProxyHttpServer()
	proxy.OnRequest().HandleConnectFunc(func(req_host string, ctx *goproxy.ProxyCtx) (*goproxy.ConnectAction, string) {
		host := req_host
		strs := strings.Split(host, ".")
		if len(strs) > 2 {
			num := len(strs) - 2
			host = strings.Join(strs[num:], ".")
		}
		if strings.HasPrefix(host, "www.") {
			host = strings.Replace(host, "www.", "", -1)
		}
		if strings.HasSuffix(host, ":443") {
			host = strings.Replace(host, ":443", "", -1)
		}
		host = strings.Trim(host, " ")
		_, isPorn := domains[host]
		log.Println("https ", host, " ", isPorn)

		if isPorn {
			return goproxy.RejectConnect, ""
		}
		return nil, ""
	})
	proxy.OnRequest().DoFunc(
		func(r *http.Request, ctx *goproxy.ProxyCtx) (*http.Request, *http.Response) {
			log.Println(r.Host)
			host := r.Host
			if strings.Contains(host, "www.") {
				host = strings.Replace(host, "www.", "", -1)
			}
			_, isPorn := domains[host]
			if isPorn {
				return r, goproxy.NewResponse(r,
					goproxy.ContentTypeText, http.StatusForbidden,
					"Don't waste your time on porn!")
			}
			return r, nil
		})
	log.Fatalln(http.ListenAndServe(port, proxy))
}
